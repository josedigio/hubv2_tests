#include <Arduino.h>

void setup() {
  Serial.begin(115200);
}

void loop() {
  delay(2000);
  Serial.printf(" ESP8266 Chip id = %08X\n", ESP.getChipId());
}
